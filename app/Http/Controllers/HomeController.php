<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;


class HomeController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function sigin(Request $request){
        $response = Http::post(route('apilogin'), [
            'email' => $request->email,
            'password' => $request->password,
        ]);
    
        $statusCode = $response->status();
        $body = $response->getBody()->getContents();
        if($statusCode==200){
            return view('home');
        }
        else{
            return view('auth.login');
        }

    }
    public function sigup(Request $request){
        dd('555');

        $response = Http::post(route('apiregister'), [
            'name'=>$request->name,
            'email' => $request->email,
            'password' => $request->password,
        ]);
    
        $statusCode = $response->status();
        $body = $response->getBody()->getContents();
        if($statusCode==200){
            return view('home');
        }
        else{
            return view('auth.register');
        }

    }
}
