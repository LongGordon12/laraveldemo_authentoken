<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\TokenGuard;
use Laravel\Sanctum\HasApiTokens;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function authenticated(Request $request, $user)
    {
        return response()->json([
            'access_token' => $user->createToken('authToken')->plainTextToken,
        ]);
    }
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', Rules\Password::defaults()],
        ]);

        if ($validate->passes()) {
            $user = User::where('email', $request->email)->first();
            if (empty($user)) {
                return response()->json(['message' => 'tài khoản không tồn tại'], 400);
            } else {
                if (!Hash::check($request->password, $user->password)) {
                    return response()->json(['message' => 'mật khẩu không đúng'], 400);
                } else {
                    Auth::login($user);
                    $token = $user->createToken('loginToken')->plainTextToken;
                    return response()->json([
                        'token' => $token,
                        'typeToken' => 'bearerToken'
                    ], 200);
                }
            }
        } else {
            return response()->json(['message' => $validate->errors()], 400);
        }
    }
    public function logout()
    {
        Auth::user()->tokens()->delete();
        Auth::logout();
        return response()->json(['message' => 'đã đăng xuất'], 200);

    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'loginToken' => '',
        ]);
        // event(new Registered($user));
        Auth::login($user);
        return response()->json(
            [
                'message' => 'Đăng ký thành công',
            ],
            200
        );
    }
}
