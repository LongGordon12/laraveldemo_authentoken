<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function showProduct(){
        return Product::all();
    }
    public function createProduct(Request $request){
        // dd($request->product_name);
        
        $currentDateTime = Carbon::now()->format('Y-m-d H:i:s');
        $validate=Validator::make($request->all(),[
            'product_name'=>['required'],
        ]);
        if($validate->passes()){
          return Product::create([
            'product_name'=>$request->product_name,
            'created_at'=>$currentDateTime,
            'updated_at'=>$currentDateTime, 
        ]);  
        }
        else{
            return response()->json(['message'=>'thêm thất bại']);

        }
        
        // return response()->json(['message' => 'Product created successfully'], 201);
    }
    public function findProduct($id){
        return Product::where('product_id', $id)->first();
    }
}
