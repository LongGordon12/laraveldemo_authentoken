<?php

namespace App\Exceptions;

use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (AuthenticationException $e, $request) {
            if ($request->is('api/*')) {
                // $check = User::where('loginToken', session('loginToken'))->first();
                // if (!session()->has('login_token') || now() >= session('login_token_expires_at') || empty($check)) {
                    return response()->json(
                        [
                            'message' => 'Phiên đăng nhập đã hết mời đăng nhập lại',
                            'errors' => 'error login'
                        ],
                        404
                    );
                // }
            }
        });
    }
}
