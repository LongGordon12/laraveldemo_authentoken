<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group( function () {
    Route::get('/product', [ProductController::class, 'showProduct']);
    Route::get('/product/{id}', [ProductController::class, 'findProduct']);
    Route::post('/product', [ProductController::class, 'createProduct']);

});
Route::get('/api_logout', [LoginController::class, 'logout'])->name('apilogout');


Route::post('/api_login', [LoginController::class, 'login'])->name('apilogin');
Route::post('/api_register', [LoginController::class, 'register'])->name('apiregister');

